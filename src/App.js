import { useState } from 'react';
import './App.css';

const App = () => {

  const [input, setInput] = useState("")

  const handleInput = (event) => {
    event.preventDefault()
    setInput(event.target.text.value)
  }

  const clearInput = () => {
    setInput("")
  }

  return (
    <div className="App">
      <h1>{input}</h1>
      <form onSubmit={e => handleInput(e)}>
        <label htmlFor="text">Insert some text here</label>
        <br/>
        <input type="text" id="text"/>
        <input type="submit" id="submit"/>
      </form>
      <button onClick={clearInput}>Clear!</button>
    </div>
  );
}

export default App;
